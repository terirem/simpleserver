package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/terirem/simpleserver/pkg/storage"
	"github.com/prometheus/client_golang/prometheus"
	uuid "github.com/satori/go.uuid"
)

type Storage interface {
	GetBalance(id string) (*storage.Wallet, error)
	PutWallet(id string) error

	GetTransactions(reqCfg storage.RequestConfig) ([]storage.Transaction, error)
	PutTransaction(storage.Transaction) error
}

type HandlersBundle struct {
	storage Storage
}

func NewBundle(s Storage) *HandlersBundle {
	return &HandlersBundle{storage: s}
}
func (hb *HandlersBundle) CreateWallet(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(handlersDurationMetric.WithLabelValues("create_wallet"))
	defer timer.ObserveDuration()

	walletID := uuid.NewV4().String()

	if err := hb.storage.PutWallet(walletID); err != nil {
		handlersCountMetric.WithLabelValues("create_wallet", "fail")
		WriteError(w, http.StatusInternalServerError, "db error")
		return
	}

	handlersCountMetric.WithLabelValues("create_wallet", "ok")
	WriteJSONObject(w, map[string]interface{}{"id": walletID})
}

// DepositWallet accepts params id and amount
func (hb *HandlersBundle) DepositWallet(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(handlersDurationMetric.WithLabelValues("deposit_wallet"))
	defer timer.ObserveDuration()

	am := r.Form.Get("amount")
	amount, err := strconv.Atoi(am)
	if err != nil || amount <= 0 {
		handlersCountMetric.WithLabelValues("depost_wallet", "fail")
		WriteAPIError(w, http.StatusBadRequest, fmt.Sprintf("wrong or missed amount : %q", am))
		return
	}

	id := r.Form.Get("id")
	if len(id) == 0 {
		handlersCountMetric.WithLabelValues("depost_wallet", "fail")
		WriteAPIError(w, http.StatusBadRequest, "missed id")
		return
	}

	err = hb.storage.PutTransaction(createTransacton(id, id, int64(amount), time.Now().Unix(), true))
	if err != nil {
		handlersCountMetric.WithLabelValues("depost_wallet", "fail")
		WriteAPIError(w, http.StatusInternalServerError, "db error")
		return
	}

	handlersCountMetric.WithLabelValues("deposit_wallet", "ok")
	w.WriteHeader(http.StatusOK)
}

// GetBalance accepts param id
func (hb *HandlersBundle) GetBalance(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(handlersDurationMetric.WithLabelValues("get_balance"))
	defer timer.ObserveDuration()

	id := r.Form.Get("id")
	if len(id) == 0 {
		handlersCountMetric.WithLabelValues("get_balance", "fail")
		WriteAPIError(w, http.StatusBadRequest, "missed id")
		return
	}

	wt, err := hb.storage.GetBalance(id)
	if err != nil {
		handlersCountMetric.WithLabelValues("get_balance", "fail")
		WriteAPIError(w, http.StatusInternalServerError, "db error")
		return
	}

	handlersCountMetric.WithLabelValues("get_balance", "ok")
	WriteJSONObject(w, wt)
}

func (hb *HandlersBundle) PutTransaction(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(handlersDurationMetric.WithLabelValues("put_transaction"))
	defer timer.ObserveDuration()

	sender := r.Form.Get("sender")
	if len(sender) == 0 {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusBadRequest, "missed sender")
		return
	}

	receiver := r.Form.Get("receiver")
	if len(receiver) == 0 {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusBadRequest, "missed receiver")
		return
	}

	am := r.Form.Get("amount")
	amount, err := strconv.Atoi(am)
	if err != nil || amount <= 0 {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusBadRequest, fmt.Sprintf("wrong or missed amount : %q", am))
		return
	}

	wt, err := hb.storage.GetBalance(sender)
	if err != nil {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusInternalServerError, "db error")
		return
	}

	if int(wt.Balance) < amount {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusBadRequest, fmt.Sprintf("usuffitient balance : %d", wt.Balance))
		return
	}

	err = hb.storage.PutTransaction(createTransacton(sender, receiver, int64(amount), time.Now().Unix(), false))
	if err != nil {
		handlersCountMetric.WithLabelValues("put_transaction", "fail")
		WriteAPIError(w, http.StatusInternalServerError, "db error")
		return
	}

	handlersCountMetric.WithLabelValues("put_transaction", "ok")
	w.WriteHeader(http.StatusOK)
}

func (hb *HandlersBundle) GetTransactions(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(handlersDurationMetric.WithLabelValues("get_transactions"))
	defer timer.ObserveDuration()

	serializator := r.Form.Get("serializator")
	if serializator != "json" || serializator != "csv" {
		serializator = "json"
	}

	var rqCfg storage.RequestConfig
	if err := json.NewDecoder(r.Body).Decode(&rqCfg); err != nil {
		handlersCountMetric.WithLabelValues("get_transactions", "fail")
		WriteAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't unmarshall config : %q", err.Error()))
		return
	}

	var resp storage.TransactionsSlice
	resp, err := hb.storage.GetTransactions(rqCfg)
	if err != nil {
		handlersCountMetric.WithLabelValues("get_transactions", "fail")
		WriteAPIError(w, http.StatusBadRequest, "db error")
		return
	}

	if serializator == "json" {
		WriteJSONObject(w, resp)
	} else {
		WriteCSVObject(w, resp.ToCSVStrings())
	}

	handlersCountMetric.WithLabelValues("get_transactions", "ok")

}

func createTransacton(sender, receiver string, amount, ts int64, ext bool) (tx storage.Transaction) {
	tx.Hash = sender + receiver + strconv.FormatInt(amount, 10) + strconv.FormatInt(ts, 10)
	tx.Sender = sender
	tx.Receiver = receiver
	tx.Amount = amount
	tx.TimeStamp = ts
	tx.External = ext
	return
}
