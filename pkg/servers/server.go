package servers

import (
	"net/http"

	"bitbucket.org/terirem/simpleserver/pkg/handlers"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

func NewMainServer(lst string, storage handlers.Storage) (srv *http.Server) {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Use(handlers.CORSHandler)

	r.Options("/*", handlers.Preflight)

	hb := handlers.NewBundle(storage)

	r.Route("/api/v1/wallet", func(r chi.Router) {
		r.Put("/create", hb.CreateWallet)
		r.Put("/deposit", hb.DepositWallet)
		r.Get("/balance", hb.GetBalance)
		r.Put("/transaction", hb.PutTransaction)
		r.Post("/listtransactions", hb.GetTransactions)
	})

	srv.Addr = lst
	srv.Handler = r

	return
}
