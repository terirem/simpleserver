package storage

var schemaUp = `
	CREATE TABLE IF NOT EXIST wallets (
		id varchar(36) PRIMARY KEY,
        balance integer,
		CONSTRAINT positive_balance CHECK (balance >= 0) 	
	);
	CREATE TABLE IF NOT EXIST transactons (
		hash varchar(128) PRIMARY KEY,
		sender varchar(36),
		receiver varchar(36),
		time_stamp timestamp,
		amount integer,
		external bool,
		CONSTRAINT positive_amount CHECK (amount >= 0) 	
		FOREIGN KEY sender REFERENCES wallets(id),
		FOREIGN KEY receiver REFERENCES wallets(id),
	);
    CREATE INDEX CONCURRENTLYIF NOT EXISTS ON transactions(sender);
	CREATE INDEX CONCURRENTLYIF NOT EXISTS ON transactions(receiver);
	CREATE INDEX CONCURRENTLYIF NOT EXISTS ON transactions(time_stamp);
	CREATE INDEX CONCURRENTLYIF NOT EXISTS ON transactions(external);
`
