package storage

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/terirem/simpleserver/pkg/loggers"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
)

type PostgreStorage struct {
	connPool *pgxpool.Pool
}

func NewPostgreStorage(addr string, log *zap.Logger) (*PostgreStorage, error) {
	cfg, err := pgxpool.ParseConfig(addr)
	if err != nil {
		return nil, err
	}
	cfg.ConnConfig.Logger = loggers.NewZapLogger(log)
	connPool, err := pgxpool.ConnectConfig(context.Background(), cfg)
	if err != nil {
		return nil, err
	}

	conn, err := connPool.Acquire(context.Background())
	if err != nil {
		connPool.Close()
		return nil, err
	}
	err = migrateUp(conn, schemaUp)
	if err != nil {
		connPool.Close()
		return nil, err
	}

	conn.Release()

	return &PostgreStorage{connPool: connPool}, nil
}

func migrateUp(cn *pgxpool.Conn, schema string) (err error) {
	_, err = cn.Exec(context.Background(), schema)
	return
}

func (s *PostgreStorage) GetBalance(id string) (wl *Wallet, err error) {
	conn, err := s.connPool.Acquire(context.Background())
	if err != nil {
		return
	}

	defer conn.Release()

	getWalletSQL := `SELECT * FROM wallets WHERE id = $1`

	row := conn.QueryRow(context.Background(), getWalletSQL, id)
	err = row.Scan(wl)
	if err != nil {
		return nil, err
	}

	return
}

func (s *PostgreStorage) PutWallet(id string) (err error) {
	conn, err := s.connPool.Acquire(context.Background())
	if err != nil {
		return
	}

	defer conn.Release()

	putWalletSQL := `INSERT INTO wallets (id) VALUES ($1)`

	result, err := conn.Exec(context.Background(), putWalletSQL, id)
	if err != nil {
		return err
	}

	if result.RowsAffected() != 1 {
		return fmt.Errorf("insertion error")
	}

	return
}

func (s *PostgreStorage) GetTransactions(reqCfg RequestConfig) (txs []Transaction, err error) {
	conn, err := s.connPool.Acquire(context.Background())
	if err != nil {
		return
	}

	defer conn.Release()

	var strSQL strings.Builder
	strSQL.WriteString("SELECT * FROM transactions")

	prms := reqCfg.ToParams()
	if len(prms) != 0 {
		strSQL.WriteString(" WHERE")
	}

	values := make([]string, len(prms))

	for i, p := range prms {
		strSQL.WriteString(p.Field)
		strSQL.WriteString("$" + strconv.Itoa(i))
		if i != len(prms)-1 {
			strSQL.WriteString(" AND ")
		}
		values = append(values, p.Value)
	}

	rows, err := conn.Query(context.Background(), strSQL.String(), values)
	if err != nil {
		return nil, err
	}

	txs = make(TransactionsSlice, 0)

	for rows.Next() {
		var tx Transaction
		err = rows.Scan(&tx.Hash, &tx.TimeStamp, &tx.Sender, &tx.Receiver, &tx.Amount, &tx.External)
		if err != nil {
			return
		}

		txs = append(txs, tx)
	}

	return
}

func (s *PostgreStorage) PutTransaction(tx Transaction) (err error) {
	conn, err := s.connPool.Acquire(context.Background())
	if err != nil {
		return
	}

	defer conn.Release()

	dtx, err := conn.Begin(context.Background())
	if err != nil {
		return err
	}

	writeSQL := `INSERT INTO transactions VALUES ($1,$2,$3,$4,$5,$6)`
	getBalanceTxSQL := `WITH debit AS
		SELECT amount FROM transactions WHERE ( sender = $1 AND external = false )
		credit AS 
		SELECT amount FROM transactions WHERE receiver = $1
		SELECT credit-debit`
	writeBalanceSQL := `INSERT INTO wallets(balance) VALUES ($1) WHERE id = $2`

	ct, err := conn.Exec(context.Background(), writeSQL, tx.Hash, tx.TimeStamp, tx.Sender, tx.Receiver, tx.Amount, tx.External)
	if err != nil {
		dtx.Rollback(context.Background())
		return err
	}

	if ct.RowsAffected() != 1 {
		dtx.Rollback(context.Background())
		return fmt.Errorf("insertion error")
	}

	var sndBal int64
	row := conn.QueryRow(context.Background(), getBalanceTxSQL, tx.Sender)
	err = row.Scan(&sndBal)
	if err == pgx.ErrNoRows {
		dtx.Rollback(context.Background())
		return fmt.Errorf("get balance error")
	}

	if sndBal < 0 {
		dtx.Rollback(context.Background())
		return fmt.Errorf("get balance error")
	}

	var rcvBal int64
	row = conn.QueryRow(context.Background(), getBalanceTxSQL, tx.Receiver)
	err = row.Scan(&rcvBal)
	if err == pgx.ErrNoRows {
		dtx.Rollback(context.Background())
		return fmt.Errorf("get balance error")
	}

	if rcvBal < 0 {
		dtx.Rollback(context.Background())
		return fmt.Errorf("get balance error")
	}

	ct, err = conn.Exec(context.Background(), writeBalanceSQL, tx.Sender, sndBal)
	if err != nil {
		dtx.Rollback(context.Background())
		return err
	}

	ct, err = conn.Exec(context.Background(), writeBalanceSQL, tx.Receiver, rcvBal)
	if err != nil {
		dtx.Rollback(context.Background())
		return err
	}

	dtx.Commit(context.Background())
	return
}
