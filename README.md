Simple HTTP server

Params (can be set by OS environment variables)

 | Name           | Default   | Meaning                       |
 | -------------- | --------- | ----------------------------- |
 | LISTEN_MAIN    | :8080     | main server listen address    |
 | LISTEN_METRICS | :8081     | metrics server listen address |
 | DSN_PG         | mandatory | DSN for Postgres instance     |

Backlog

* Testing
* Integration tests based on SQLite
* Dockerize this and orchestrate with compose
