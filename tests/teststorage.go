package tests

import (
	"context"
	"database/sql"
	"log"

	"bitbucket.org/terirem/simpleserver/pkg/storage"
)

func MakeTestStorage() error {
	st, err := storage.NewSQLiteStorage("testdb.sqlite")
	if err != nil {
		return err
	}

	conn, err := st.GetConnect()
	if err != nil {
		return err
	}

	defer conn.Close()

	migrate(conn)
	insertTxs(conn)

	return nil
}

func migrate(conn *sql.Conn) error {
	var sqls []string

	sqls = append(sqls, `CREATE TABLE transactions (
		txHash TEXT NOT NULL PRIMARY KEY,		
		timestamp INTEGER NOT NULL,
		sender TEXT NOT NULL,
		recipient TEXT NOT NULL,
		amount INTEGER NOTNULL		
	)`)

	sqls = append(sqls, `CREATE INDEX ts_index ON transactions(timestamp)`)
	sqls = append(sqls, `CREATE INDEX snd_index ON transactions(sender)`)
	sqls = append(sqls, `CREATE INDEX ts_index ON transactions(recipient)`)

	log.Println("Create transactions table...")
	for _, sql := range sqls {
		_, err := conn.ExecContext(context.Background(), sql)
		if err != nil {
			return err
		}
	}
	log.Println("transactions table created")

	return nil

}

func insertTxs(db *sql.Conn) error {
	// insert test rows
	// insertTxsSQL := `INSERT INTO transactions`
	return nil
}
