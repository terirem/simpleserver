module bitbucket.org/terirem/simpleserver

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/jackc/pgx/v4 v4.11.0
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/prometheus/client_golang v1.11.0
	github.com/satori/go.uuid v1.2.0
	go.uber.org/zap v1.17.0
)
