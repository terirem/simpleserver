package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"bitbucket.org/terirem/simpleserver/pkg/loggers"
	"bitbucket.org/terirem/simpleserver/pkg/servers"
	"bitbucket.org/terirem/simpleserver/pkg/storage"
	"go.uber.org/zap"
)

func main() {
	if err := loggers.NewLogger(); err != nil {
		log.Fatal(err)
	}

	lstmain := os.Getenv("LISTEN_MAIN")
	if len(lstmain) == 0 {
		lstmain = ":8080"
	}

	lstmetrics := os.Getenv("LISTEN_METRICS")
	if len(lstmetrics) == 0 {
		lstmetrics = ":8081"
	}

	dsnPG := os.Getenv("DSN_PG")
	if len(dsnPG) == 0 {
		zap.L().Fatal("Postgre DSN is not set")
	}

	storage, err := storage.NewPostgreStorage(dsnPG, zap.L().Named("postgre"))
	if err != nil {
		zap.L().Fatal("Postgre init", zap.Error(err))
	}

	wg := &sync.WaitGroup{}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())

	go servers.Serve(ctx, servers.NewMainServer(lstmain, storage), zap.L().With(zap.String("server", "main")), wg)

	go servers.Serve(ctx, servers.NewMetricsServer(lstmetrics, nil), zap.L().With(zap.String("server", "metrics")), wg)

	sig := <-c
	zap.L().Info("system interrupt", zap.Stringer("signal", sig))
	cancel()
}
